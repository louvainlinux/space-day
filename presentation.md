---
marp: true
theme: uncover
_class: invert
---
<div style="position: absolute; top: 50px; left: 90px;">
    <img src="images/kot_astro.png" alt="Kot Astro" style="width: 250px;">
</div>

<div style="position: absolute; top: 50px; right: 90px;">
    <img src="images/tux_llnux.png" alt="Louvain-li-Nux" style="width: 200px;">
</div>

# Space Day

## L'informatique spatiale

Par le Louvain-li-Nux

---
<div style="position: absolute; top: 50px; left: 70px;">
    <img src="images/tux_llnux.png" alt="Louvain-li-Nux" style="width: 100px;">
</div>

## Qui sommes-nous ?

Le Louvain-li-Nux est un kot à projet qui promeut l'utilisation du libre et de l'open source en informatique.

---
<div style="position: absolute; top: 50px; left: 70px;">
    <img src="images/tux_llnux.png" alt="Louvain-li-Nux" style="width: 100px;">
</div>


## Qu'est-ce que c'est ?
**Libre** : Garanti à vie -> exécuter, copier, modifier et distribuer librement.
**Open-source** : le code source est accessible à tous et tout le monde peut contribuer.

---
<div style="position: absolute; top: 50px; left: 70px;">
    <img src="images/tux_llnux.png" alt="Louvain-li-Nux" style="width: 100px;">
</div>


## Trois modules

<div style="display: flex; justify-content: space-between;">
    <div style="border: 3px solid black; padding: 10px; border-radius: 15px; margin-right:30px; border-color:red; background:rgb(240, 65, 46, 0.4)">
        <h3>Module 1</h3>
        De la Lune à l'informatique quantique
    </div>
    <div style="border: 3px solid black; padding: 8px 10px 15px 10px; border-radius: 15px; margin-right:30px ;border-color:blue; background:rgb(52, 100, 235, 0.4);">
        <h3>Module 2</h3>
        Utilisations des technologies informatique dans l'espace
    </div>
    <div style="border: 3px solid black; padding: 10px; border-radius: 15px; border-color:green; background-color: rgb(37, 156, 51, 0.4)">
        <h3>Module 3</h3>
        Les premiers algorithmes spatiaux
    </div>
</div>

---
<div style="position: absolute; top: 50px; left: 70px;">
    <img src="images/tux_llnux.png" alt="Louvain-li-Nux" style="width: 100px;">
</div>

<div style="">
    <img src="images/members.webp" alt="Members" style="width: 900px;">
</div>

Merci et bonnes découvertes !

